# Read Me

## Foreword

Hello!

This is an example student blog for the [2023-2024 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/).

Each student has a personal GitLab remote repository in the [GitLab students group](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students) in which a template of a blog is already stored. 

The GitLab software  is set to use [MkDocs](https://www.mkdocs.org/) to turn simple text files written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

#### Edit your blog

There are several ways to edit your blog

* by navigating your remote GitLab repository and [editing the files using the GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#edit-a-file).
* by using [git](https://rogerdudler.github.io/git-guide/) and Command Line on your computer, clone a local copy of your online project repository, edit the files locally and synchronize back your local repository with the copy on the remote server.

#### Publishing your blog

Two times a week and each time you change a file, your website is rebuilt and all the changes are published in few minutes.

#### It saves history

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

#### Be frugal to spare the planet

However, be frugal in the size of files that you upload and keep your website lightweight.  No need to upload images with full resolution, for a large image, 1024px in width is more than enough. 