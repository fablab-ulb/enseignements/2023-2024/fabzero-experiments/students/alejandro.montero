$fn=1000;
// BISTAB

//proportionality of the forms
a=1;
//epsilon
eps=0.1;
//Height of th epiecs
h=5;
rotate([180,0,0]){
//Cylindre extérieur
difference(){
    cylinder(a*(h),a*30, a*30, center=true);
    cylinder(a*(h+eps),a*25, a*25, center=true);
}
//Cylindre intérieur
cylinder(a*(h), a*10, a*10, center=true);

// Pins d'attache
translate([a*5,0,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
translate([0,a*5,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
translate([0,a*-5,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
translate([a*-5,0,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
}


//FLEXLINKS
//angles of the flexlink
r=10;
//width of the flexlinks
w=0.5;
//lengths of the flexlinks
l=30*a-10*a;
//deepness of the flexlinks
d=a*(h/2);
translate([a*19,0,-1.25]){
    rotate([0,0,r]) {
        cube([l,w,d],center=true);
    }
}
translate([0,a*19,-1.25]){
    rotate([0,0,90+r]) {
        cube([l,w,d],center=true);
    }
}
translate([0,a*-19,-1.25]){
    rotate([0,0,90+r]) {
        cube([l,w,d],center=true);
    }
}
translate([a*-19,0,-1.25]){
    rotate([0,0,r]) {
        cube([l,w,d],center=true);
    }
}






//FREEWHEEL
translate([-50,-70,1.4]){
h1=7.5;
r1=30;
r2=r1-5;
h2=5;
h3=1.6;
r3=15;
dzcentre=h2/2-h1/2+1.5;
dzbase=-h1/2+h3/2;
eps=0.1;
//dx=0;
dx=r1*2.3;
//flexlink parameter
a=1;
//angles of the flexlink
r=235;
//width of the flexlinks
w=0.5;
//lengths of the flexlinks
l=30*a-8*a;
//deepness of the flexlinks
d=a*5;

//EXTERN RATCHET
union(){
difference(){
cylinder(h1,r1,r1,center=true);
cylinder(h1+eps,r2,r2,center=true);
}
translate([a*21,-1,-1.25]){
    rotate([0,0,r-10]) {
        cube([l,w,d],center=true);
    }
}
translate([1,a*21,-1.25]){
    rotate([0,0,90+r-10]) {
        cube([l,w,d],center=true);
    }
}
translate([-1,a*-21,-1.25]){
    rotate([0,0,90+r-10]) {
        cube([l,w,d],center=true);
    }
}
translate([a*-21,1,-1.25]){
    rotate([0,0,r-10]) {
        cube([l,w,d],center=true);
    }
}
}

//INTERN RATCHET
union(){
translate([dx,0,dzcentre])
    difference(){
        cylinder(h2*2-2,r3,r3,center=true);
        union(){
            translate([a*5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*-5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([a*-5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
        }
    }
translate([14+dx,0,dzcentre])rotate([0,0,80])cube([l-5.5,w,h2*2-2],center= true);
translate([-14+dx,0,dzcentre])rotate([0,0,260])cube([l-5.5,w,h2*2-2],center= true);
translate([0+dx,14,dzcentre])rotate([0,0,160])cube([l-5.5,w,h2*2-2],center= true);
translate([0+dx,-14,dzcentre])rotate([0,0,-10])cube([l-5.5,w,h2*2-2],center= true);
    
    
//BASE SUPPORT
    difference(){
    difference(){
    union(){
    difference(){
translate([dx,0,-h1/2+(h1-4)/2])cylinder(h1-4,r1+4,r1+4,center=true);
translate([dx,0,-h1/2+(h1-4)/2])cylinder(h1-4+eps,r2+7,r2+7,center=true);
}
translate([dx,0,dzbase])cylinder(h3,r1+2,r1+2,center=true);
}
}

translate([dx,0,dzcentre])union(){
            translate([a*5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*-5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([a*-5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
        
    }

}
}
}
