# 2. Computer-Aided Design (CAD)
The content in this module covers the process of creating my own project in *CAD*, detailing how I interacted with the program and concluded with a mechanism that I was satisfied with. Since it wasn't my first time working with 3D modeling programs, I didn't find this experience very challenging. However, I thoroughly enjoyed it, as I have with every other instance of working with modeling programs. We were given the opportunity to work with three different 2D and 3D *CAD* tools, and I chose to work with OpenSCAD because I found it very intuitive.

## My own compilant mechanism *Freewh33l*
My project is a *freewheel*, a mechanism commonly found among the main bicycle parts. It allows the pedals to move freely in one direction but opposes movement in the other. This mechanism is composed of two parts: a base with an exterior small wall to keep the wheel in place and one central monolith, which opposes the movement; and a wheel with flexible tabs inside that fit with the other part, enabling the mechanism to work.


### Creation process

- The easiest part seemed to be the wheel, so I started with that. To do so, I designed a cylinder and made a hole in it with the same form but with a smaller radius.
![](images/Open_SCAD_-_Creacion_1.png)

- The tabs were created by designing four prisms with one dimension substantially larger than the other two, adding some rotation, and merging them with the wheel. They are oriented not toward the center of the wheel to fit properly with the other part of the mechanism.
![](images/Open_SCAD_-_Creacion_2.png)

- The base was more challenging to make. I began by creating the central piece, joining one large cylinder with four triangular prisms. The prisms were constructed using the cylinder function but with a defined number of edges. They were also given a specific angle so that they could fit with the tabs.
![](images/Open_SCAD_-_Creacion_3.png)

- At first, I had several problems figuring out how the number of edges of the cylinder worked. The solution was to include this information inside the cylinder brackets instead of adding it afterwards.
![](images/Open_SCAD_-_Error_1.png)

- After adding one cylinder as the floor and walls, the base was completely finished.
![](images/Open_SCAD_-_Creacion_4.png)

- As we had to create a small prototype for next week, I combined both pieces in the same document so that the 3D printer could print them at the same time.
![](images/Open_SCAD_-_Creacion_5.png)


### Complete code

```
/ File : Freewh33l.scad 
// Author : Alejandro Montero Lahoz
// Date : 03/11/2023 
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1) 


h1=10;
r1=30;
r2=r1-5;
h2=15;
h3=1.6;
r3=6;
dzcentre=h2/2-h1/2;
dzbase=-h1/2+h3/2;
//dx=0;
dx=r1*2.3;
//flexlink parameter
a=1;
//angles of the flexlink
r=10;
//width of the flexlinks
w=0.5;
//lengths of the flexlinks
l=30*a-8*a;
//deepness of the flexlinks
d=a*5;
union(){
difference(){
cylinder(h1,r1,r1,center=true);
cylinder(h1,r2,r2,center=true);
}
translate([a*18,0,0]){
    rotate([0,0,r]) {
        cube([l,w,d],center=true);
    }
}
translate([0,a*18,0]){
    rotate([0,0,90+r]) {
        cube([l,w,d],center=true);
    }
}
translate([0,a*-18,0]){
    rotate([0,0,90+r]) {
        cube([l,w,d],center=true);
    }
}
translate([a*-18,0,0]){
    rotate([0,0,r]) {
        cube([l,w,d],center=true);
    }
}
}
union(){
translate([dx,0,dzcentre])cylinder(h2,r3,r3,center=true);
translate([4+dx,0,dzcentre])rotate([0,0,20])cylinder(h2,r3/1.2,r3/1.2,center= true,$fn=3);
translate([-4+dx,0,dzcentre])rotate([0,0,200])cylinder(h2,r3/1.2,r3/1.2,center= true,$fn=3);
translate([0+dx,4,dzcentre])rotate([0,0,110])cylinder(h2,r3/1.2,r3/1.2,center= true,$fn=3);
translate([0+dx,-4,dzcentre])rotate([0,0,-70])cylinder(h2,r3/1.2,r3/1.2,center= true,$fn=3);
    difference(){
translate([dx,0,-h1/2+(h1-4)/2])cylinder(h1-4,r1+4,r1+4,center=true);
translate([dx,0,-h1/2+(h1-4)/2])cylinder(h1-4,r2+7,r2+7,center=true);
}
translate([dx,0,dzbase])cylinder(h3,r1+2,r1+2,center=true);
}
```
## Acknowledgments

For this project, I received help from my classmate [Théo Grijspeerdt](https://theo-grijspeerdt-fablab-ulb-enseignements-2023-2-966d6998506ad4.gitlab.io/) and his coding abilities to model the flexible tabs. You can also find his work in this module [here](https://theo-grijspeerdt-fablab-ulb-enseignements-2023-2-966d6998506ad4.gitlab.io/fabzero-modules/module02/).

This is the code he sent to start from:
```
// File : Circ_Bistab.scad 
// Author : Théo Grijspeerdt 
// Date : 16/10/2023 
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 

    //proportionality of the forms 
    a=1; 
    //epsilon 
    eps=0.1; 

    //Cylindre extérieur 
    difference(){ 
        cylinder(a*12,a*30, a*30, center=true); 
        cylinder(a*12+eps,a*25, a*25, center=true); 
    } 
    //Cylindre intérieur 
    translate([0,0,a*1]){ 
        difference(){ 
            cylinder(a*14, a*10, a*10, center=true); 
            cylinder(a*14+eps,a*5,a*5, center=true); 
        } 
    } 


    //FLEXLINKS 
    //angles of the flexlink 
    r=10; 
    //width of the flexlinks 
    w=0.5; 
    //lengths of the flexlinks 
    l=30*a-10*a; 
    //deepness of the flexlinks 
    d=a*5; 

    translate([a*19,0,0]){ 
        rotate([0,0,r]) { 
            cube([l,w,d],center=true); 
        } 
    } 
    translate([0,a*19,0]){ 
        rotate([0,0,90+r]) { 
            cube([l,w,d],center=true); 
        } 
    } 
    translate([0,a*-19,0]){ 
        rotate([0,0,90+r]) { 
            cube([l,w,d],center=true); 
        } 
    } 
    translate([a*-19,0,0]){ 
        rotate([0,0,r]) { 
            cube([l,w,d],center=true); 
        } 
    }  
```