# 1. Project management and documentation
## Foreword

The content in this module represents my learning process on how to manage projects in this course and become familiar with working on Gitlab, the site we are going to use during this semester to document our progress in various activities and technologies.

## 1. First steps on Gitlab
First of all, we had to create an individual account on [Gitlab](https://gitlab.com/). After following some easy steps and entering our username as instructed by the teacher, we could begin exploring our private space on Gitlab. The primary goal of the first project is to personalize our main page on the FabZero website, and for that, we were provided with a list of tutorials to follow.


The primary objective of the first project is to personalize our main page on the [FabZero](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/) website, and for that, we had a list of tutorials to follow.

## 2. Working with GitLab
During this week's classes, we learned that we can program on *Gitlab* either directly on the internet or on our computers locally. One of the main objectives of this module is to acquire proficiency in programming in both environments.

### 2.1. Online Programming
From my point of view, coding on the internet is the easiest option and also the most intuitive. Once you reach [Gitlab](https://gitlab.com/) and log into your account, you just have to click on the upper left corner of the page for the menu to appear. Then click on *Projects* where you can see the projects in which you can participate.

![](images/project-panagement-21-online-programming-1.jpg)

Next, navigate to the project you want to modify, click on *Edit*, and choose the option *Open in Web IDE*.

![](images/project-management-21-online-programming-2.jpg)

From this website, you can directly modify the information you want to display on your own site. Each time you want to save your progress, be sure to click on the menu on the left, select the *Source Control* icon (the third one from the top), and then click on *Commit to 'main'* to ensure you don't lose your progress.

![](images/project-management-21-online-programming-3.jpg)

Now, your progress is secure, and you don't have to worry in case you lose your internet connection or your computer turns off. 
### 2.2. Local Programming
Using this method has the advantage that everything is done through *Linux* programs, which means that we don't use a graphical user interface (GUI), and the programs execute faster.

#### Installation

To work locally, first of all, you have to make sure that you have *Git* installed on your computer. In order to do so, open your command prompt and write this:

```
git --version
```
If you don't have it installed this mensaje will appear:

![](images/Project_Management_-_Creacion_1.png)

To install Git on your computer, you have to go to the [Git Website](https://git-scm.com/), click on *Download for Windows*, and follow the steps.

Now that you have *Git* installed, let's start with the configuration. Open *git-bash* and introduce this command to create your username:
```
git config --global user.name "Alejandro"
```
and then your email adress:
```
git config --global user.email "a.mlahoz@alumnos.upm.es"
```
To verify that everything you have done is alright, you can run this command:
```
git config --global --list
```

#### My own SSH key
An *SSH key*, or Secure Shell key, is a cryptographic key pair used to authenticate and secure communication between two computers over a network. It comprises a public key, shared with others, and a private key, kept confidential. SSH keys are commonly employed for secure remote access to systems, file transfers, and other secure network operations. The private key proves the user's identity, while the public key is used to verify that identity.

Before starting the creation of the key, we have to check if we already have an *SSH Key*. In order to do so, write: ```
/ssh ```. If the folder doesn't exist, we have to create it. Use the following command to create the new key:
```
ssh-keygen -t rsa -b 2048 -C "comment"
```
The `-t rsa` specifies the type of key to create; in this case, the 'rsa' indicates that the RSA algorithm will be used to generate the key pair. The `-b 2048` sets the number of bits in the key, and `-C "comment"` adds a comment to the key. This is what you should get in return:
```
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user/.ssh/rsa):
```
After selecting where you want your key to be stored, you can choose a password.

Your SSH key is comprised of a pair of files: a private key and a public key. To work with *GitLab*, you need to upload it. Open your profile page on *GitLab*, and in the left menu, click on *SSH keys*. Click on *Add a new key*. Navigate to the folder where your key documents are located, copy the public key, and click on *Add a new key*. Now you have a functioning SSH key.

#### Cloning my workspace on my device
In order to work from your device, you have to copy all the documents you want to work with into a folder on your computer. Go to your course project main page and click on *Clone with SSH*. Then run this command:
```
git clone git@gitlab.com:fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/alejandro.montero.git
```
#### Markdown format and text editors
Now that you have your workspace on your device, you need a tool to work on your files. This tool is called a *text editor*, and there are several options. I have chosen to use VSCodium. You can follow [this link](https://vscodium.com/) to get it. To install it, you need to download the latest version. You can find it by clicking on *download latest release* on the website. As a *Windows* user, I will get the *VSCodiumSetup-x64-1.83.1.23285.exe or VSCodium-x64-1.83.1.23285.msi* version. To finish this, I just had to run this command line in my command prompt:
```
winget install vscodium
```

#### Uploading files to the archive
Now, we have everything that we need to start working. To navigate to your repository, use ```cd "example"``` in the command prompt. Once you are there, write:
```
git pull
```
This command will download the modifications that have been made on GitLab. To avoid conflicts while working with more people, make sure that you run this command first. If you are working alone, you should get ```Already up-to-date.```

Now you can modify your files using your text editor. Before closing anything, make sure that you save your progress. To update the files in *GitLab*, run this command:
```
git add -A
```
Writing *-A* will update all the files. To update only some of them, type their names after *add*. If you run ```git status```, a list of the updated files in green will be displayed.

To push the modifications to *GitLab*, run:
```
git push
```

Now you know how to work with *GitLab* both locally from your computer and online. Choose the method that fits you better.

## 3. Compressing Images
Loading several images involves a waste of energy that can be reduced if we change the definition of the image to one that is coherent with the information we want to convey. We have to take care of the planet as much as we can, and even if it doesn't do much, this helps. Additionally, it contributes to my website loading faster.

There are many ways to compress images, but I'm going to do it with GraphicsMagick, which allows me to modify images using commands.

If you want to do the same as me, first, install [GraphicsMagick](http://www.graphicsmagick.org/). Choose the image you want to compress in your command window. Navigate to the corresponding folder with ```cd```, then run this command:
```
gm convert -resize 400 "originphoto".jpg "finalphoto".png
```
The parts of this code are:
- ```gm``` indicates the use of *GraphicsMagick*.
- ```convert``` is used for image conversions and operations.
- ```-resize 400``` indicates that the width of the new image is set to 400 pixels.
- ```"originphoto".jpg``` is the name of the source image file to be processed.
- ```"finalphoto".png``` is the name of the destination file.
## 4. My website
"To create my website, I have used the various tools that Git offers. By varying the number of hashes written before a sentence, titles and subtitles can be created. For example, ```## My website```. Text can also be modified by changing its font to ```*italic*``` or ```**bold**```. An unordered list can be created by writing ```*``` or ```-``` at the beginning of each line. The list won't generate unless one empty line is left between the elements of the list."

Links can be implemented by writing this type of command `[Text](link)`. The text written between the square brackets will be visible, but the link won't.


To add images, they have to be uploaded to a folder situated in the same directory as the project you are working on. Here you have an example `![Left menu](images/project-management-21-online-programming-2.jpg)`. It took me a while to figure out exactly how this worked and at first I was loading the images in a folder that was one level higher than this module. The text between square brackets won't appear; it can be used to help yourself while programming, and the route to the image is written between parentheses.

"To attach documents, they have to be uploaded to a folder too. Here you have an example `[My document](file.txt)`. In this case, the information between square brackets will appear, and the route to the file is written between parentheses."

## 5. Project Management

There are several ways to optimize the way work is done, and in this unit, we have learned some of them, of which I have selected a few.

### Spiral Development

Spiral development is akin to creating something by progressing in loops. Instead of doing everything simultaneously, we plan a bit, create a small piece, evaluate its progress, learn from it, and then repeat the process. It's like taking little steps and learning from each one before moving forward. This way, we can adjust and improve things as we go, especially when we're not certain about everything at the start.

This is the method that suits me the most, and it's the one I've used most of the time to develop my projects.

### Supply-side Time Management

Supply-side time management is akin to creating a schedule based on your tasks and the available time. Instead of just considering your availability, you strategically plan your time to complete tasks efficiently, essentially organizing your day around what needs to be done.

This principle is incredibly helpful in our course. Given that we're initiating new projects before completing the current ones, it's easy to neglect the others if we concentrate too much on just one. In such a scenario, we might have to go back and redo everything from the start, resulting in a significant waste of time.
