# 3. 3D Printing

The content in this module documents the trial-and-error process I followed to create a 3D printed version of the mechanism I designed in the [second module](https://alejandro-montero-fablab-ulb-enseignements-2023--3a253f28b22529.gitlab.io/fabzero-modules/module02/) of this course. It outlines how I iteratively modified the design until I was satisfied with the result. This was my first experience working with 3D printers, and I thoroughly enjoyed it. I would recommend it to anyone who has the chance. It is incredibly satisfying to see a project you've designed become a reality.

## 3.1. From the computer to the printer

- To 3D print your project, you need to create a file that the 3D printer can read. To do this, open your project in *OpenSCAD* and press *F6* to render your model. Then press *F7* to save your project as an *STL* file.

- Next, open the *PrusaSlicer* program and click on the first icon in the upper menu to load your *STL* file.
![](images/Impresión_3D_-_Creacion_1.jpg)

- Now, you will be able to visualize your object on the screen. Complete the data that the program is asking you for in the right menu, such as filament and printer. Make sure to choose the right ones; if you don't, the printer could overheat and do nothing, as it happened to me when I selected the wrong filament. In our case, we are going to use *PLA filament* and an *Original Prusa i3 MK3S* printer. If you don't want to struggle collecting your object after the printing has finished, select the *Brim* option, and the printer will add some more filament around the object.
![](images/Impresión_3D_-_Creacion_2.jpg)

- Click on *Slice now* to see how long the printing will take, how every part is going to be made, and the simulation of the impression. By clicking on *Export G-code*, you will have your file ready to start the printing.
![](images/Impresión_3D_-_Creacion_3.jpg)

## 3.2. The evolution of my mechanism
As I explained in my [second FabZero module](https://alejandro-montero-fablab-ulb-enseignements-2023--3a253f28b22529.gitlab.io/fabzero-modules/module02/), my mechanism is a freewheel, a device commonly found among the main bicycle parts. It allows the pedals to move freely in one direction but opposes movement in the other. This mechanism is composed of two parts: a base with an exterior small wall to keep the wheel in place and one central monolith, which opposes the movement; and a wheel with flexible tabs inside that fit with the other part, allowing the mechanism to work. The evolution from the first version to the final one has been determined by a process of trial and error. Every time a new problem emerged, a new version solving it was created.

### V1

- The first version of my mechanism that I printed is shown in my [second FabZero module](https://alejandro-montero-fablab-ulb-enseignements-2023--3a253f28b22529.gitlab.io/fabzero-modules/module02/). In order to print for the first time, we were asked to make a version of our mechanism that would be completely printed in under 30 minutes. To reduce the time of printing, I emptied the monolith of the base piece. However, the printing didn't go as I planned, and the tabs of the wheel collapsed while they were being printed. From this first version, I learned that all parts of my design must start being printed from the floor. In this first design, the tabs started some millimeters from the floor.
![](images/Impresión_3D_-_Error_2.jpeg)

### V2

- To solve the previous problem with the tabs, I lowered them so that the impression would start from the floor. This time, I only printed the wheel of the mechanism to reduce the quantity of PLA and time that I was using. When the print was finished, it seemed that everything was in good condition. After checking that everything was correctly printed, it was time to try the mechanism. Unfortunately, the tabs weren't long enough, and the angle of the bumps of the monolith wasn't sufficient to prevent the wheel from turning in both directions.
![](images/Impresión_3D_-_Error_3.jpeg)

### V3

- For this version, I lengthened the bars, increased their angle, and also increased the angle of the monoliths. This time I printed the piece with the real dimensions that I wanted it to have. Due to the excessive thinness of the tabs, the printer couldn't manage to print them in place, and at the end of the printing time, the piece was useless. The absence of a photo is due to my oversight as I forgot to take one before recycling the PLA.

### V4

- To make it easier for the printer to print the tabs and to add some resistance to the wheel-tabs bond, I increased the thickness to be 3 times bigger. To make this bond even greater, I added more thickness to the lower part of the wheel. Only the wheel was printed this time. At the end of the procedure, everything seemed right, but after using the mechanism during the day, it started to feel loose. After a couple more turns, one of the tabs broke down.
![](images/Impresión_3D_-_Error_5.jpeg)

### V5 (Final Version)
- For this version, I decided to completely change the design of the base because it wasn't working as well as I wanted in the last version. The triangular prisms of the monolith were changed for long squared bars that could fulfill their opposing purpose in a more efficient way and also allow the turn in the desired direction. The bond of the tabs and the wheel was reinforced by adding some more material in one of the corners of the wheel-tab union, and the length of the tabs was slightly reduced. After the printing, the model was completely finished, and the mechanism worked perfectly.
![](images/Impresión_3D_-_Final_1.jpg)
![](images/Impresión_3D_-_Final_2.jpg)

Here you have some pictures of the final result of my mechanism:

![](images/Impresión_3D_-_Final_3.jpeg)
![](images/Impresión_3D_-_Final_4.jpeg)

## 3.3. Conecting 2 pieces

Applying what we have learned in class, my colleague Thèo and I have come up with a way to join two of our pieces and print them. Thèo's mechanism is called bistability. Our idea is that by joining our parts together, you can somehow see if the force being applied to the freewheel is too strong.

![](images/Impresión_3D_-_Final_5.jpeg)
![](images/Impresión_3D_-_Final_6.jpeg)

## 3.4. What I have learned from testing the 3D printers

During these days working with the 3D printers, I learned several things that I didn't realize at first, even if they were quite obvious:

- You have to choose the right type of filament you are going to print on.

- Not properly cleaning the surface where the object is going to be printed could lead to bad printing.

- Not all parts of a piece will have the same internal structure.

- If a part is too thin, the printer will not be able to print it correctly.

## 3.5. My files

Here you have the .stl files of the machanism and the one that I did with Thèo:

- [Base.stl](files/Base.stl)

- [Wheel.stl](files/Rueda.stl)

- [2_Pieces.stl](files/2piezas.stl) 

Here you have Thèo and I common project code:
```
$fn=1000;
// BISTAB

//proportionality of the forms
a=1;
//epsilon
eps=0.1;
//Height of th epiecs
h=5;
rotate([180,0,0]){
//Cylindre extérieur
difference(){
    cylinder(a*(h),a*30, a*30, center=true);
    cylinder(a*(h+eps),a*25, a*25, center=true);
}
//Cylindre intérieur
cylinder(a*(h), a*10, a*10, center=true);

// Pins d'attache
translate([a*5,0,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
translate([0,a*5,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
translate([0,a*-5,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
translate([a*-5,0,-1.5]){
    cylinder(5,2.3,2.3,center=true);
}
}


//FLEXLINKS
//angles of the flexlink
r=10;
//width of the flexlinks
w=0.5;
//lengths of the flexlinks
l=30*a-10*a;
//deepness of the flexlinks
d=a*(h/2);
translate([a*19,0,-1.25]){
    rotate([0,0,r]) {
        cube([l,w,d],center=true);
    }
}
translate([0,a*19,-1.25]){
    rotate([0,0,90+r]) {
        cube([l,w,d],center=true);
    }
}
translate([0,a*-19,-1.25]){
    rotate([0,0,90+r]) {
        cube([l,w,d],center=true);
    }
}
translate([a*-19,0,-1.25]){
    rotate([0,0,r]) {
        cube([l,w,d],center=true);
    }
}






//FREEWHEEL
translate([-50,-70,1.4]){
h1=7.5;
r1=30;
r2=r1-5;
h2=5;
h3=1.6;
r3=15;
dzcentre=h2/2-h1/2+1.5;
dzbase=-h1/2+h3/2;
eps=0.1;
//dx=0;
dx=r1*2.3;
//flexlink parameter
a=1;
//angles of the flexlink
r=235;
//width of the flexlinks
w=0.5;
//lengths of the flexlinks
l=30*a-8*a;
//deepness of the flexlinks
d=a*5;

//EXTERN RATCHET
union(){
difference(){
cylinder(h1,r1,r1,center=true);
cylinder(h1+eps,r2,r2,center=true);
}
translate([a*21,-1,-1.25]){
    rotate([0,0,r-10]) {
        cube([l,w,d],center=true);
    }
}
translate([1,a*21,-1.25]){
    rotate([0,0,90+r-10]) {
        cube([l,w,d],center=true);
    }
}
translate([-1,a*-21,-1.25]){
    rotate([0,0,90+r-10]) {
        cube([l,w,d],center=true);
    }
}
translate([a*-21,1,-1.25]){
    rotate([0,0,r-10]) {
        cube([l,w,d],center=true);
    }
}
}

//INTERN RATCHET
union(){
translate([dx,0,dzcentre])
    difference(){
        cylinder(h2*2-2,r3,r3,center=true);
        union(){
            translate([a*5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*-5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([a*-5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
        }
    }
translate([14+dx,0,dzcentre])rotate([0,0,80])cube([l-5.5,w,h2*2-2],center= true);
translate([-14+dx,0,dzcentre])rotate([0,0,260])cube([l-5.5,w,h2*2-2],center= true);
translate([0+dx,14,dzcentre])rotate([0,0,160])cube([l-5.5,w,h2*2-2],center= true);
translate([0+dx,-14,dzcentre])rotate([0,0,-10])cube([l-5.5,w,h2*2-2],center= true);
    
    
//BASE SUPPORT
    difference(){
    difference(){
    union(){
    difference(){
translate([dx,0,-h1/2+(h1-4)/2])cylinder(h1-4,r1+4,r1+4,center=true);
translate([dx,0,-h1/2+(h1-4)/2])cylinder(h1-4+eps,r2+7,r2+7,center=true);
}
translate([dx,0,dzbase])cylinder(h3,r1+2,r1+2,center=true);
}
}

translate([dx,0,dzcentre])union(){
            translate([a*5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([0,a*-5,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
            translate([a*-5,0,-1.5]){
                cylinder(15,2.4,2.4,center=true);
            }
        
    }

}
}
}

```