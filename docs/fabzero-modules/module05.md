# 5. Team dynamics and final projet

In this section, I will provide an overview of the various group work days we've had during the course.

## 5.1. Project Analysis and Design
In this project, we were tasked with drawing inspiration from [fablab projects](https://class-website-fablab-ulb-enseignements-2023-2024-c4a40531a3337f.gitlab.io/#archives) and [frugal science](https://gitlab.com/fablab-ulb/enseignements/fabzero/projects/-/blob/main/open-source-lab.md), either individually or in pairs. The assignment involved selecting a project of personal interest, analyzing it, and constructing both a problem tree and a solution tree. I collaborated on this project with my classmate, [Thèo](https://theo-grijspeerdt-fablab-ulb-enseignements-2023-2-966d6998506ad4.gitlab.io/), and together, we chose to address the issue of scientific equipment in the laboratory.
![](images/Team_dynamics_and_final_project_-_Problem_tree.png)
![](images/Team_dynamics_and_final_project_-_Solutions_tree.png) 

## 5.2. Group formation and brainstorming on project problems
For this session, we were tasked with bringing an object to class that symbolized a present-day issue. I chose a piece of plastic found on the street to highlight the pervasive problem of global pollution caused by human activities. During the class, we gathered and placed our objects on the ground, allowing everyone to examine them. We engaged in discussions to connect with people who shared similar or intriguing issues. Subsequently, we formed groups and delved into detailed explanations of our chosen objects and the associated problems.

In the end, I joined a group comprised of:

- [Emilie Cellier](https://emilie-cellier-fablab-ulb-enseignements-2023-202-28ebbb8f9c2c98.gitlab.io/)

- [Oubayda Edleby](https://oubayda-edleby-fablab-ulb-enseignements-2023-202-262a81649773e0.gitlab.io/fabzero-modules/module05/)

- [Thèo Grijspeerdt](https://theo-grijspeerdt-fablab-ulb-enseignements-2023-2-966d6998506ad4.gitlab.io/)

Due to the different themes and objects, we chose Energy as the common topic.

We discussed more specific topics such as the storage of energy, electricity supply in Lebanon, and the gap in the production capacity-proportion of renewable energies use.

After discussing these topics with each other, we were given an A3 sheet of paper to write the visible/invisible parts of our chosen problems. Then, we chose one person among us to explain our topics to the other groups, while representatives from other groups explained theirs to us so that we could help them approach their topics from another perspective. When we were done with this, we gathered and chose which topics interested each one more.

Another day, we gathered and created a Problem Tree to address the various themes we had discussed the day before and to delve as deeply as possible into finding the root of the problem. We were given an A3 sheet and spent most of the class discussing the cause-and-effect relationships. We ended up with this tree:
![](images/Team_dynamics_and_final_project_-_Problems_1.jpeg)
![](images/Team_dynamics_and_final_project_-_Problems_2.jpeg)

Thanks to this activity, we ended up with a list of possible challenges to address:

- Creation of a non-electric sustainable lighting device

- Refrigerator design to reduce energy loss when opened

- Tea heating using solar radiation

- Development of a non-electric and user-friendly elevator for tall buildings

- Strategies for mitigating consumption peaks without disturbing the user

I think that, while this dynamic is useful to a certain extent, in longer sessions like the ones we have in this subject lasting 2 hours, it becomes less effective. Eventually, focus is lost, and progress slows down.

## 5.3. Group dynamics

### Cumulative voting

Cumulative voting is a voting method in which individuals are provided with a fixed number of votes that they can distribute among different candidates or allocate all to a single candidate. This approach allows voters the flexibility to express strong support for specific candidates. Cumulative voting is frequently employed in group decision-making scenarios with multiple options, enabling individuals to articulate their preferences clearly. We utilized this method during the brainstorming session on project problems to determine the topics that resonated most with us.

![](images/Team_dynamics_and_final_project_-_Problems_3.jpeg)

### Charter and rituals

Establishing a group charter and traditions in student teamwork is vital for clear communication, defining roles, and cultivating a positive team environment. The charter outlines responsibilities and conflict resolution, fostering accountability among members. Regular meetings and celebrations serve as ongoing practices, providing stability and boosting motivation. This structured approach ensures a shared understanding, contributing to effective collaboration. Ultimately, it nurtures an environment where team members feel engaged and committed to achieving common objectives.

Rituals in student groupwork serve to foster a positive team culture and provide a structured routine for communication and collaboration.These are the ones we chose:

1. Review the charter.

2. Feedback on the work done since the last meeting.

3. Discussion of feelings since the last meeting.

4. Explanation of the plan for the current meeting.

5. Work time.

6. Planning of tasks and roles for the next week.

7. Feedback on this meeting.

### Turning roles

Rotating roles in student group work is crucial because it encourages fair participation, distributes responsibilities equally, and enables each member to develop various skills. This approach helps prevent burnout and nurtures a collaborative and inclusive team atmosphere.

