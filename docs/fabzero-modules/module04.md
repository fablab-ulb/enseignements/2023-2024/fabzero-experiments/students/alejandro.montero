# 4. Selected Fab Lab tool

For this module, we were given the option to choose between Computer Controlled Cutting and 3D Printing for Repair; and Electronic Prototyping. I chose Electronic Prototyping because it seemed the most interesting one for me, and I had never worked with this kind of technology. The main goal of this unit was to learn how to use a microcontroller development board to prototype electronic applications by programming it and using input and output devices.

In order to work, we needed to install [Arduino IDE](https://www.arduino.cc/en/software) on our computers to code. We were given an electronic kit composed of an *RP2040 development board* and other elements to work with and develop our skills.

During the class sessions, we programmed our board to perform specific tasks. Most of the time was devoted to working autonomously and learning from the problems that we faced.

## 4.1. RP2040 development board

A microcontroller is a small integrated circuit that incorporates a processor, memory, and input/output peripherals, tailored for precise control functions in electronic devices such as household appliances and automotive systems. The board we were given is called *RP2040* and these are its components: 
![](images/Electronic_Prototyping_-_Mother_board.png)


## 4.2. NeoPixel - Addressable RGB LED

The objective of this exercise was to write a program to assign a specific color to the RGB LED. To fulfill this purpose, we installed the [Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel) library on *Arduino IDE*. This library contains several examples that can be modified to display a chosen color on the RGB LED. By modifying the three numbers in this line, `pixels.setPixelColor(i, pixels.Color(0, 255, 255))`, you can choose a color from the entire spectrum; in this case, cyan. The required circuit for this program was the simplest possible – we just had to plug the board into our computer with the USB-C cable we were given. Doing this exercise took me a while because the board wasn't being recognized. The problem solved itself after plugging and unplugging the cable several times. This error didn't repeat. If wanted, there is the possibility to alternate between light colors; this is where `DELAYVAL` comes into play. The units are in milliseconds, so with `DELAYVAL 500`, the light changes every half second. Choosing other colors is as easy as adding the following lines of code as many times as the number of wanted colors into `void loop()`:
```
pixels.setPixelColor(i, pixels.Color(0, 255, 255));
pixels.show(); 
 delay(DELAYVAL);
```

Here is the code that I used:

```
#include <Adafruit_NeoPixel.h>
#define PIN 23
#define NUMPIXELS 1 
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 500

void setup() {
  pixels.begin(); 
}

void loop() {
  pixels.clear(); 
 
  pixels.setPixelColor(i, pixels.Color(0, 255, 255));
  pixels.show(); 
  delay(DELAYVAL);
  
}
```

## 4.3. DHT20 - Temperature & RH sensor

The objective of this exercise was to connect a sensor to our development board and write a program that could give results as an output. I installed the *DHT20 library* and read the sensor datasheet, where it explained where the connections had to be attached.

This is the layout that I used for this exercise:

![](images/Electronic_Prototyping_-_Luz.jpeg)

And this is the code:
```
//
//    FILE: DHT20_plotter.ino
//  AUTHOR: Rob Tillaart
// PURPOSE: Demo for DHT20 I2C humidity & temperature sensor
//
//  Always check datasheet - front view
//
//          +--------------+
//  VDD ----| 1            |
//  SDA ----| 2    DHT20   |
//  GND ----| 3            |
//  SCL ----| 4            |
//          +--------------+


#include "DHT20.h"

DHT20 DHT(&Wire);


void setup()
{
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();  
  //  ESP32 default pins 21 22
  Serial.begin(115200);
  Serial.println("Humidity, Temperature");
}

void loop()
{
  if (millis() - DHT.lastRead() >= 1000)
  {
    //  note no error checking
    DHT.read();
    Serial.print(DHT.getHumidity(), 1);
    Serial.print(", ");
    Serial.println(DHT.getTemperature(), 1);
  }
}
```
## 4.4. Servo Motor

In the last exercise, we had to use an analog knob to adjust the rotation speed of a servo motor. To successfully build this circuit, we had to embrace the difference between analog and digital signals. While analog signals are continuous waves, digital signals are represented by discrete values. For this exercise, I installed the [Servo library](https://www.arduino.cc/reference/en/libraries/servo/) to control the motor.

This is the layout that I used for this exercise:

![](images/Electronic_Prototyping_-_Motor.jpeg)

And this is the code:
```
#include <Servo.h>
Servo myservo;

int potpin = A1;
int val;

void setup() {
  myservo.attach(7);
}

void loop() {
  val = analogRead(potpin);
  val = map(val, 0, 1023, 0, 180);
  myservo.write(val);
  delay(20)
}
```