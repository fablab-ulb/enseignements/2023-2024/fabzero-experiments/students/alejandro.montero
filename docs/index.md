## Foreword

Hello! This is Alejandro Montero's student blog for the [2023-2024 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/).
Thank to this blog you will be capable to follow how my journey through this course. I hope you like what you see!
## About me

![](images/avatar-photo.jpg)

Hi! I am Alejandro Montero. I am a civil engineering student from Spain and I am currently in Brussels on Erasmus program.

Visit this website to see my work!

## My background

I was born in a nice city called Zaragoza where I spent my childhood until I turned 18 and then moved to Madrid. In Madrid I chose to study civil engineering in the Universidad Politécnica de Madrid due to my interests in mathematics, physics and the building environement. Last year I felt the urge to explore the possibilities that studying in another country could provide me and decided to apply to the Erasmus program.

## Previous work

During Summer 2023 I made an internship in [Ensaya©](https://ensaya.es/), a geological and geotechnical test company in Zaragoza, where I improved my GIS management habilities.

## My Hobbies

I love sports, I have practiced several of them such as basketball, handball, rugby and volleyball, but the one that I enjoy the most is snowboarding. I also enjoy a lot playing videogames, my favourite ones are the "soulslike", for example: *Dark Souls III* and *Elden Ring*.